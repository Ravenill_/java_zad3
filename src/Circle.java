import java.awt.*;
import java.awt.event.MouseEvent;
import java.lang.Math.*;

public class Circle implements Figure
{
    private Point_2D center;
    private float radius;

    public Circle(Point_2D center, float radius)
    {
        this.center = center;
        this.radius = radius;
    }

    public void setCenter(Point_2D center)
    {
        this.center = center;
    }

    public void setRadius(float radius)
    {
        this.radius = radius;
    }

    public Point_2D getCenter()
    {
        return center;
    }

    public float getRadius()
    {
        return radius;
    }

    public void draw(Graphics g)
    {
        g.drawOval((int)center.x, (int)center.y, (int)radius, (int)radius);
    }

    public boolean isInside(MouseEvent event)
    {
        int mouseX = event.getX();
        int mouseY = event.getY();
        double mouseDistance = Math.sqrt(Math.pow(Math.abs(mouseX - (center.x + radius/2)), 2)  + Math.pow(Math.abs(mouseY - (center.y + radius/2)), 2));

        if (mouseDistance <= radius)
            return true;
        else
            return false;
    }

    public void print()
    {
        System.out.println("Circle, " + center.x + ", " + center.y + ", " + radius);
    }

    public void moveLeft(int move)
    {
        center.x = center.x - move;
    }

    public void moveRight(int move)
    {
        center.x = center.x + move;
    }

    public void moveUp(int move)
    {
        center.y = center.y - move;
    }

    public void moveDown(int move)
    {
        center.y = center.y + move;
    }
}
