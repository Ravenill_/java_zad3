import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.*;

public class DrawPanel extends JPanel implements MouseListener
{
    private ArrayList<Figure> figureList;
    private Figure actualFigure;

    public DrawPanel()
    {
        super();
        //addMouseListener(this);

        actualFigure = null;
        figureList = new ArrayList<>();
        setPreferredSize(new Dimension(400, 400));

        FigureFactory factory = new FigureFactory();
        figureList.add(factory.generateSquare(new Point_2D(10, 10), new Point_2D(100, 100)));
        figureList.add(factory.generateCircle(new Point_2D(250, 250), 100));
        figureList.add(factory.generateTriangle(new Point_2D(200, 200), new Point_2D(300, 300), new Point_2D(300, 100)));
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        for(Figure figure : figureList)
        {
            figure.draw(g);
        }
    }

    public Figure getActualFigure()
    {
        return actualFigure;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e) == true)
        {
            for(Figure figure : figureList)
            {
                if (figure.isInside(e))
                {
                    actualFigure = figure;
                }
            }
            if (actualFigure != null)
            {
                actualFigure.print();
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }
}
