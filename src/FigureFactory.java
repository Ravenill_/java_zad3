import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class FigureFactory
{
    public Square generateSquare(Point_2D a, Point_2D b)
    {
        return new Square(a, b);
    }

    public Triangle generateTriangle(Point_2D a, Point_2D b, Point_2D c)
    {
        return new Triangle(a, b, c);
    }

    public Circle generateCircle(Point_2D center, float radius)
    {
        return new Circle(center, radius);
    }

    public Square generateRandomSquare()
    {
        Random generator = new Random();
        return new Square(new Point_2D(generator.nextFloat(), generator.nextFloat()), new Point_2D(generator.nextFloat(), generator.nextFloat()));
    }

    public Triangle generateRandomTriangle()
    {
        Random generator = new Random();
        return new Triangle(new Point_2D(generator.nextFloat(), generator.nextFloat()), new Point_2D(generator.nextFloat(), generator.nextFloat()), new Point_2D(generator.nextFloat(), generator.nextFloat()));
    }

    public Circle generateRandomCircle()
    {
        Random generator = new Random();
        return new Circle(new Point_2D(generator.nextFloat(), generator.nextFloat()), generator.nextFloat());
    }
}