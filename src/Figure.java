import java.awt.*;
import java.awt.event.MouseEvent;

public interface Figure
{
    void draw(Graphics g);
    boolean isInside(MouseEvent event);
    void print();
    void moveLeft(int move);
    void moveRight(int move);
    void moveUp(int move);
    void moveDown(int move);
}
