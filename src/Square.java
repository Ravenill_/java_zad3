import java.awt.*;
import java.awt.event.MouseEvent;
import java.lang.Math.*;

public class Square implements Figure
{
    private Point_2D a, b;

    public Square(Point_2D a, Point_2D b)
    {
        this.a = a;
        this.b = b;
    }

    public Point_2D getA()
    {
        return a;
    }

    public Point_2D getB()
    {
        return b;
    }

    public void setA(Point_2D a)
    {
        this.a = a;
    }

    public void setB(Point_2D b)
    {
        this.b = b;
    }

    public void draw(Graphics g)
    {
        g.drawLine((int)a.x, (int)a.y, (int)b.x, (int)a.y);
        g.drawLine((int)b.x, (int)a.y, (int)b.x, (int)b.y);
        g.drawLine((int)b.x, (int)b.y, (int)a.x, (int)b.y);
        g.drawLine((int)a.x, (int)b.y, (int)a.x, (int)a.y);
    }

    public boolean isInside(MouseEvent event)
    {
        int mouseX = event.getX();
        int mouseY = event.getY();

        if (mouseX > a.x && mouseX < b.x)
        {
            if (mouseY > a.y && mouseY < b.y)
                return true;
        }

        return false;
    }

    public void print()
    {
        System.out.println("Square, " + a.x + ", " + a.y + ", " + b.x + ", " + b.y);
    }

    public void moveLeft(int move)
    {
        a.x = a.x - move;
        b.x = b.x - move;
    }

    public void moveRight(int move)
    {
        a.x = a.x + move;
        b.x = b.x + move;
    }

    public void moveUp(int move)
    {
        a.y = a.y - move;
        b.y = b.y - move;
    }

    public void moveDown(int move)
    {
        a.y = a.y + move;
        b.y = b.y + move;
    }
}
