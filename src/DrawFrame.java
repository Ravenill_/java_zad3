import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

public class DrawFrame extends JFrame implements KeyListener
{
    private JPanel panel;
    private DrawPanel myPanel;

    public DrawFrame()
    {
        super("Rysowanie");
        panel = new DrawPanel();
        myPanel = (DrawPanel)panel;

        add(panel);
        addKeyListener(this);
        addMouseListener((MouseListener)panel);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void keyPressed(KeyEvent evt) {
        int key = evt.getKeyCode();

        switch(key)
        {
            case KeyEvent.VK_RIGHT:
                myPanel.getActualFigure().moveRight(10);
                System.out.println("RIGHT");
                break;
            case KeyEvent.VK_LEFT:
                myPanel.getActualFigure().moveLeft(10);
                System.out.println("LEFT");
                break;
            case KeyEvent.VK_UP:
                myPanel.getActualFigure().moveUp(10);
                System.out.println("UP");
                break;
            case KeyEvent.VK_DOWN:
                myPanel.getActualFigure().moveDown(10);
                System.out.println("DOWN");
                break;
        }
        myPanel.revalidate();
        myPanel.repaint();
    }

    @Override
    public void keyReleased(KeyEvent evt) {

    }

    @Override
    public void keyTyped(KeyEvent evt) {
    }
}

