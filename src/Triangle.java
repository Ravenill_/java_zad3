import java.awt.*;
import java.awt.event.MouseEvent;
import java.lang.Math.*;

public class Triangle implements Figure
{
    private Point_2D a, b, c;

    public Triangle(Point_2D a, Point_2D b, Point_2D c)
    {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Point_2D getA()
    {
        return a;
    }

    public void setA(Point_2D a)
    {
        this.a = a;
    }

    public Point_2D getB()
    {
        return b;
    }

    public void setB(Point_2D b)
    {
        this.b = b;
    }

    public Point_2D getC()
    {
        return c;
    }

    public void setC(Point_2D c)
    {
        this.c = c;
    }

    public void draw(Graphics g)
    {
        g.drawLine((int)a.x, (int)a.y, (int)b.x, (int)b.y);
        g.drawLine((int)b.x, (int)b.y, (int)c.x, (int)c.y);
        g.drawLine((int)c.x, (int)c.y, (int)a.x, (int)a.y);
    }

    float sign (Point_2D p1, Point_2D p2, Point_2D p3)
    {
        return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
    }

    public boolean isInside(MouseEvent event)
    {
        int mouseX = event.getX();
        int mouseY = event.getY();
        Point_2D mousePoint = new Point_2D(mouseX, mouseY);

        boolean b1, b2, b3;

        b1 = sign(mousePoint, a, b) < 0.0f;
        b2 = sign(mousePoint, b, c) < 0.0f;
        b3 = sign(mousePoint, c, a) < 0.0f;

        return ((b1 == b2) && (b2 == b3));
    }

    public void print()
    {
        System.out.println("Triangle, " + a.x + ", " + a.y + ", " + b.x + ", " + b.y + ", " + c.x + ", " + c.y);
    }

    public void moveLeft(int move)
    {
        a.x = a.x - move;
        b.x = b.x - move;
        c.x = c.x - move;
    }

    public void moveRight(int move)
    {
        a.x = a.x + move;
        b.x = b.x + move;
        c.x = c.x + move;
    }

    public void moveUp(int move)
    {
        a.y = a.y - move;
        b.y = b.y - move;
        c.y = c.y - move;
    }

    public void moveDown(int move)
    {
        a.y = a.y + move;
        b.y = b.y + move;
        c.y = c.y + move;
    }
}
